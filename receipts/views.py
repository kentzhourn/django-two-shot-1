from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from receipts.models import Receipt, Account, ExpenseCategory

# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    template_name = "receipts/list.html"
    model = Receipt
    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

 
class ReceiptAccountsCreateView(LoginRequiredMixin, CreateView):
    template_name = "receipts/accounts_create.html"
    model = Account
    fields = ["name", "number"]
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("receipt_account_list")


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    template_name = "receipts/categories_create.html"
    model = ExpenseCategory
    fields = ["name"]
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("receipt_category_list")



class ReceiptCreateView(LoginRequiredMixin, CreateView):
    template_name = "receipts/create.html"
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        return redirect("home")

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    template_name = "receipts/categories_list.html"
    model = ExpenseCategory
    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

class AccountListView(LoginRequiredMixin, ListView):
    template_name = "receipts/accounts_list.html"
    model = Account
    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

  
