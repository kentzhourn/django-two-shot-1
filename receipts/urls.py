from django.urls import path
from receipts.views import (
    ReceiptListView, ReceiptAccountsCreateView, ExpenseCategoryCreateView, ReceiptCreateView, ExpenseCategoryListView, AccountListView
 
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/",
        ReceiptAccountsCreateView.as_view(),
        name="accounts_create",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="categories_create",
    ),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path("categories/", ExpenseCategoryListView.as_view(), name="receipt_category_list"),
    path("accounts/", AccountListView.as_view(), name="receipt_account_list")
]
